#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define FOO_TYPE_BAR (foo_bar_get_type ())

G_DECLARE_FINAL_TYPE (FooBar, foo_bar, FOO, BAR, GObject);

/**
 * foo_bar_new
 *
 * returns: (transfer full)
 */
FooBar   *foo_bar_new         ();
void      foo_bar_set_number  (FooBar  *self,
                               gint     value);
gint      foo_bar_get_number  (FooBar  *self);

G_END_DECLS
