#include "foo-bar.h"

struct _FooBar
{
  GObject parent_instance;
  gint    number;
};

G_DEFINE_TYPE (FooBar, foo_bar, G_TYPE_OBJECT);

enum
{
  _UNUSED,
  PROP_NUMBER,
  N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject      *gobj,
              guint         prop_id,
              const GValue *value,
              GParamSpec   *spec)
{
  FooBar *self;

  self = FOO_BAR (gobj);

  switch (prop_id)
    {
    case PROP_NUMBER:
      foo_bar_set_number (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, prop_id, spec);
    }
}

static void
get_property (GObject    *gobj,
              guint       prop_id,
              GValue     *value,
              GParamSpec *spec)
{
  FooBar *self;

  self = FOO_BAR (gobj);

  switch (prop_id)
    {
    case PROP_NUMBER:
      g_value_set_int (value, self->number);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, prop_id, spec);
    }
}

static void
foo_bar_init (FooBar *self)
{
}

static void
foo_bar_class_init (FooBarClass *cls)
{
  GObjectClass *gcls;

  gcls = G_OBJECT_CLASS (cls);
  gcls->set_property = set_property;
  gcls->get_property = get_property;

  /*
   * Here we get an error caused by vapigen
   * 
   * g-ir-scanner doesn't set object parent property 
   * then vapigen says FooBar is not an object so it
   * cannot have constructed properties
   *
   * It seems to work on gobject-introspection 1.56.1
   * But doesn't on 1.60 and 1.61 with newest glib
   *
   * After removing G_PARAM_CONSTRUCT the problem is gone
   */
  props[PROP_NUMBER] =
    g_param_spec_int ("number",
                      "number",
                      "An example property",
                      G_MININT32, G_MAXINT32, 42,
                      G_PARAM_CONSTRUCT | G_PARAM_READWRITE);

  g_object_class_install_properties (gcls, N_PROPS, props);
}

void
foo_bar_set_number (FooBar *self,
                    gint    value)
{
  if (value != self->number)
    {
      self->number = value;
      g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NUMBER]);
    }
}

gint
foo_bar_get_number (FooBar *self)
{
  return self->number;
}
