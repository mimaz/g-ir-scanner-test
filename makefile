all: foo.vapi

clean:
	rm -f libfoo.so foo.gir foo.vapi

ARGS=`pkg-config --libs --cflags gobject-2.0`

libfoo.so: foo-bar.c foo-bar.h
	gcc ${ARGS} -fPIC -shared $< -o $@

foo.gir: libfoo.so
	g-ir-scanner foo-bar.h foo-bar.c --namespace=Foo --library=foo ${ARGS} -L. --output foo.gir

foo.vapi: foo.gir
	vapigen --library=foo foo.gir
